function warning(msg) {
    return `
    <div class="alert alert-warning" role="alert">
    Sorry nothing is showing! Try again.
    </div>`
  }

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';       
    
    try {
        const response = await fetch(url);
                
        if (!response.ok)   {
            throw new Error('Response not ok')
        } else {
            const data = await response.json();

            const selectTag = document.getElementById('location');
            for (let location of data.locations) {
                const option = document.createElement('option');            
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.appendChild(option);
            }
            const formTag = document.getElementById('create-conference-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData.entries()));
                const conferenceUrl = 'http://localhost:8000/api/conferences/';
                const fetchConfig = {
                    method: "POST",
                    body: json,
                    headers: {
                    'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();                
                    const newConference = await response.json();                
                };
            })
        }   
    } catch (e) {
        console.error('error', e);
        const alert = warning(e.msg)
        const row = document.querySelector(".form-control")
        row.innerHTML = alert + row.innerHTML;
        }    
    
});


