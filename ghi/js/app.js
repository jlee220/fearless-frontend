function createCard(name, location, description, pictureUrl, isoStarts, isoEnds) {
  return ` 
  <div>
    <div class="shadow p-3 mb-5 bg-body rounded">
      <img src="${pictureUrl}" class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
          </div>  
          <div class="card-footer">
          ${isoStarts} - ${isoEnds}   
          </div>       
    </div>
  </div>
  `;
}

function warning(msg) {
  return `
  <div class="alert alert-warning" role="alert">
  Sorry it's broken!
  </div>`
}

window.addEventListener('DOMContentLoaded', async () => {
  
  const url = 'http://localhost:8000/api/conferences/';

  try {
    const response = await fetch(url);

    if (!response.ok) {
      throw new Error('Response not ok');      
    } else {
      const data = await response.json();

      for (let conference of data.conferences) {
        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          const name = details.conference.name;
          const location = details.conference.location.name;
          const description = details.conference.description;
          const pictureUrl = details.conference.location.picture_url;
          const startDate = new Date(details.conference.starts);
          const isoStarts = `${startDate.getMonth()+1}-${startDate.getDate()}-${startDate.getFullYear()}`;
          const endDate = new Date(details.conference.ends);
          const isoEnds = `${endDate.getMonth()+1}-${endDate.getDate()}-${endDate.getFullYear()}`;

          const html = createCard(name, location, description, pictureUrl, isoStarts, isoEnds);
          
          const column = document.querySelector('.row');
          column.innerHTML += html;     
        }      
      }
    }
  } catch (e) {
    console.error('error', e);
    const alert = warning(e.msg)
    const row = document.querySelector(".row")
    row.innerHTML = alert + row.innerHTML;
  }  

});
